﻿const app = require("application");
const http = require("http");


function AppNext() {
	this.appNextId = null;
};





AppNext._getIdfa = function() {
	return new Promise((resolve, reject) => {
		net.pubnative.AdvertisingIdClient.getAdvertisingId(app.android.context, new net.pubnative.AdvertisingIdClient.Listener({
			onAdvertisingIdClientFinish: (adInfo) => {
				resolve(adInfo.getId(), null);
			},
			onAdvertisingIdClientFail: (error) => {
				resolve(null, error);
			}
		}));
	});
}
/*
AppNext.getRawData = function(placemendID, count) {
	return new Promise((resolve, reject) =>{
		AppNext._getIdfa().then((adInfo, error) => {
			let url = 'https://global.appnext.com/offerWallApi.aspx?pimg=1&cnt='+count+'&tid=API&id='+placemendID;
			if (adInfo !== null) {
				url += '&did='+adInfo;
			}
			http.getJSON(url).then(response => {
				resolve(response);
			}, err => {
				reject(err);
			});	
		});
	});
}

*/

AppNext.prototype.init = function(id, type) {
	this.appNextId = id;
	switch(type) {
		case 'interstitial':
			this.instance = new com.appnext.ads.interstitial.Interstitial(app.android.foregroundActivity, id);
		break;
		case 'fullScreenVideo':
			this.instance = new com.appnext.ads.fullscreen.FullScreenVideo(app.android.foregroundActivity, id);
			this.instance.setShowClose(true);
		break;
		case 'rewardedVideo':
			this.instance = new com.appnext.ads.fullscreen.RewardedVideo(app.android.foregroundActivity, id);
		break;
	}
};


AppNext.prototype.loadAd = function() {
	this.instance.loadAd();
};

AppNext.prototype.showAd = function() {
	this.instance.showAd();
};








AppNext.prototype.onAdLoaded = function() {
	return new Promise((resolve, reject) => {
		this.instance.setOnAdLoadedCallback(
			new com.appnext.core.callbacks.OnAdLoaded(
			{
				adLoaded: () => {
					resolve();
				}
			})
		);
	});
};


AppNext.prototype.onAdOpened = function() {
	return new Promise((resolve, reject) => {
		this.instance.setOnAdOpenedCallback(
			new com.appnext.core.callbacks.OnAdOpened(
			{
				adOpened: () => {
					resolve();
				}
			})
		);
	});
};

AppNext.prototype.onAdClicked = function() {
	return new Promise((resolve, reject) => {
		this.instance.setOnAdClickedCallback(
			new com.appnext.core.callbacks.OnAdClicked(
			{
				adClicked:() => {
					resolve();
				}
			})
		);
	});
};

AppNext.prototype.onAdClosed = function() {
	return new Promise((resolve, reject) => {
		this.instance.setOnAdClosedCallback(
			new com.appnext.core.callbacks.OnAdClosed(
			{
				onAdClosed: () => {
					resolve();
				}
			})
		);
	});
};

AppNext.prototype.onAdError = function() {
	return new Promise((resolve, reject) => {
		this.instance.setOnAdErrorCallback(
			new com.appnext.core.callbacks.OnAdError(
			{
				adError: (err) => {
					resolve(err);
				}
			})
		);
	});
};


AppNext.prototype.onAdEnded = function() {
	return new Promise((resolve, reject) => {
		this.instance.setOnVideoEndedCallback(
			new com.appnext.core.callbacks.OnVideoEnded(
			{
				videoEnded: (err) =>{
					resolve(err);
				}
			})
		);
	});
};


module.exports = AppNext;

