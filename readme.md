# NativeScript AppNext plugin


For now only working with android. (Javascript only)
Content api:

  - Interstitial
  - Rewarded/Fullscreen Video

# Usage
```
const AppNext = require('nativescript-appnext');
global.appNext = new AppNext();

//Raw data
AppNext.getRawData('PLACEMEND_ID', count).then(ads => { }); //count = how many ads you want get. // DISABLED 1.0.1


//Init
global.appNext.init('PLACEMENT_ID', 'interstitial');//instead of intersitial can be: fullScreenVideo or rewardedVideo

//Load (optional)
global.appNext.loadAd();

//showAd
global.appNext.showAd();

//Now callbacks:

global.appNext.onAdLoaded().then(() => { });

global.appNext.onAdOpened().then(() => { });

global.appNext.onAdClicked().then(() => { });

global.appNext.onAdClosed().then(() => { });

global.appNext.onAdEnded().then(() => { }); //Video Only

```
